Name:             ssh-chat
Version:          1.10 
Release:          0.5
Group:            Productivity/Networking/SSH
Summary:          SSH server providing a chatroom
License:          MIT
URL:              https://github.com/shazow/ssh-chat
#Original source: https://github.com/shazow/ssh-chat/archive/v1.10.tar.gz
#Custom source, renamed for Go vendor dependencies and including launch script as well as systemd unit file below:
Source0:          %{name}-%{version}.tar.gz
Source1:          vendor.tar.gz
%if 0%{?suse_version}
BuildRequires:    go >= 1.16
BuildRequires:    golang-packaging systemd-rpm-macros fdupes rpm
Requires:         systemd
%endif
%if 0%{?suse_version} > 1500
BuildRequires:    openssh-common
Requires:         openssh-common
%endif
%if 0%{?sle_version} == 150300
BuildRequires:    openssh-common
Requires:         openssh-common
%endif
%if 0%{?sle_version} == 150200
BuildRequires:    openssh
Requires:         openssh
%endif
%if 0%{?fedora}
BuildRequires:    golang go-rpm-macros fdupes openssh
Requires:         systemd openssh
%endif

%define __arch_install_post export NO_BRP_STRIP_DEBUG=true

%global provider_prefix github.com/shazow/ssh-chat
%global import_path %{provider_prefix} 
%if 0%{?fedora} || 0%{?0%{?centos_version}}
%global goipath github.com/shazow/ssh-chat
%global debug_package %{nil}
%endif

%if 0%{?suse_version}
%{go_nostrip}
%{go_provides}
%endif
%if 0%{?fedora}
%gopkg
%endif

%description
Custom SSH server written in Go. Instead of a shell, you get a chat prompt.

%prep
%setup -q -n %{name}-%{version}
%setup -q -T -D -a 1
%if 0%{?fedora}
%goprep
%endif
%setup -q -T -D -a 1

%pre
%service_add_pre ssh-chat.service

%build
export GOFLAGS="-mod=vendor"
%if 0%{?suse_version}
%goprep %{import_path}
%gobuild cmd/ssh-chat 
%endif
#Hacking Fedora's Go packaging implementation to use vendor source files instead of nuking them... Know a better way? PLEASE let me know.
%if 0%{?fedora}
rm -rf "%{_builddir}/%{extractdir0}/_build/src/github.com/shazow/ssh-chat"
ln -s "%{_builddir}/%{name}-%{version}" "%{_builddir}/%{extractdir0}/_build/src/github.com/shazow/ssh-chat"
%gobuild -o %{gobuilddir}/bin/%{name} %{goipath}/cmd/ssh-chat
%endif

%install
export GOFLAGS="-mod=vendor"
%if 0%{?suse_version}
%goinstall
%gosrc
%endif

%if 0%{?suse_version}
%fdupes %{buildroot}%{_datadir}/go
install -d -m 755 %{buildroot}%{_sbindir}
ln -s /usr/sbin/service %{buildroot}%{_sbindir}/rcssh-chat
%endif
%if 0%{?fedora}
install -m 0755 -vd %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/
%endif
%ifarch x86_64
install -D -m 700 %{_builddir}/%{name}-%{version}/rpm/ssh-chat64.sh %{buildroot}%{_sysconfdir}/%{name}/ssh-chat.sh
%else
install -D -m 700 %{_builddir}/%{name}-%{version}/rpm/ssh-chat.sh %{buildroot}%{_sysconfdir}/%{name}/ssh-chat.sh
%endif
install -d -m 700 %{buildroot}%{_libdir}/%{name}
ssh-keygen -f %{buildroot}%{_libdir}/%{name}/host_key -P ''
chmod 400 %{buildroot}%{_libdir}/%{name}/host_key
touch %{buildroot}%{_sysconfdir}/%{name}/admin_keys
chmod 600 %{buildroot}%{_sysconfdir}/%{name}/admin_keys
install -D -m 644 %{_builddir}/%{name}-%{version}/rpm/ssh-chat.service %{buildroot}%{_unitdir}/ssh-chat.service

%check
%if 0%{?suse_version}
export GOFLAGS="-mod=vendor"
# Architecture related testing issues: https://github.com/shazow/ssh-chat/issues/187
%ifarch i568
%gotest %{import_path}/cmd/ssh-chat
%ifarch armv7l
echo "No Go testing possible!"
%else 
%gotest %{import_path}
%endif
%endif
%endif
%if 0%{?fedora}
%ifarch armv7l
echo "No Go testing possible!"
%else
%gocheck
%endif
%endif

%if 0%{?suse_version}
%gofilelist
%files -f file.lst
%{_sbindir}/rcssh-chat
%endif
%if 0%{?fedora}
%files
%endif
%{_bindir}/%{name}
%license LICENSE
%doc README.md
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/ssh-chat.sh
%config(noreplace) %{_sysconfdir}/%{name}/admin_keys
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/host_key
%{_libdir}/%{name}/host_key.pub
%{_unitdir}/ssh-chat.service

%post
%service_add_post ssh-chat.service
%preun
%service_del_preun ssh-chat.service
%postun
%service_del_postun ssh-chat.service

%changelog
* Fri Jun 04 2021 Georg Pfuetzenreuter <mail@georg-pfuetzenreuter.net> - 1.10-0.5
- Added symlink to 32bit libdir on 64bit platforms to prevent issue with the hostkey
* Wed Mar 10 2021 Georg Pfuetzenreuter <mail@georg-pfuetzenreuter.net> - 1.10-0.4
- Fixed a typo. Added Group. Experimenting with CentOS support.
* Tue Mar 09 2021 Georg Pfuetzenreuter <mail@georg-pfuetzenreuter.net> - 1.10-0.3
- Added support for additional architectures: openSUSE Tumbleweed i586 and openSUSE ARM armv7l
* Tue Mar 09 2021 Georg Pfuetzenreuter <mail@georg-pfuetzenreuter.net> - 1.10-0.2
- Added support for Fedora
* Sun Mar 07 2021 Georg Pfuetzenreuter <mail@georg-pfuetzenreuter.net> - 1.10-0.1
- First ssh-chat package
